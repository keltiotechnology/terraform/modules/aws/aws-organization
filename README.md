<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
# Aws organizations

## Usage

```hcl-terraform
provider "aws" {}

module "aws-s3-static-website-cloudfront-with-ovh-dns" {
  source                         = "git::https://gitlab.com/keltiotechnology/terraform/infras/aws-keltio-organization"

  create_org                     = true

  create_organizational_units    = true

  organizational_units           = [
  {
   name      = "Sandbox",
   parent_name = ""
  },
  {
   name      = "Workloads",
   parent_name = ""
  },
  {
   name      = "Prod",
   parent_name = "Workloads"
  }
 ]

  create_organizations_accounts  = true

  organizations_accounts         = [
  {
   name      = "account_name",
   email     = "account@email.com",
   parent_name = "Sandbox", (doesn't work with nested OUs)
   role_name = ""
  },
 ]
```

## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.56.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_organizations_account.accounts](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/organizations_account) | resource |
| [aws_organizations_organization.keltio](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/organizations_organization) | resource |
| [aws_organizations_organizational_unit.nested_ous](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/organizations_organizational_unit) | resource |
| [aws_organizations_organizational_unit.root_ous](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/organizations_organizational_unit) | resource |
| [aws_organizations_policy.policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/organizations_policy) | resource |
| [aws_organizations_policy_attachment.policy_attachment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/organizations_policy_attachment) | resource |
| [aws_organizations_organization.keltio](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/organizations_organization) | data source |
| [aws_organizations_organizational_units.all_ous](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/organizations_organizational_units) | data source |
| [aws_organizations_organizational_units.ou](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/organizations_organizational_units) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_service_access_principals"></a> [aws\_service\_access\_principals](#input\_aws\_service\_access\_principals) | List of AWS service principal names for which you want to enable integration with your organization | `list(any)` | `[]` | no |
| <a name="input_create_org"></a> [create\_org](#input\_create\_org) | This variable controls if Organization will be created or not | `bool` | `false` | no |
| <a name="input_create_organizational_units"></a> [create\_organizational\_units](#input\_create\_organizational\_units) | This variable controls if Organizations Organizational unit will be created or not | `bool` | `false` | no |
| <a name="input_create_organizations_accounts"></a> [create\_organizations\_accounts](#input\_create\_organizations\_accounts) | This variable controls if Organizations Accounts will be created or not | `bool` | `false` | no |
| <a name="input_create_organizations_policies"></a> [create\_organizations\_policies](#input\_create\_organizations\_policies) | This variable controls if Organizations Policies will be created or not | `bool` | `false` | no |
| <a name="input_enabled_policy_types"></a> [enabled\_policy\_types](#input\_enabled\_policy\_types) | List of Organizations Policy Types to enable in the Organization Root | `list(any)` | `[]` | no |
| <a name="input_feature_set"></a> [feature\_set](#input\_feature\_set) | Specify 'ALL' (default) or 'CONSOLIDATED\_BILLING' | `string` | `"ALL"` | no |
| <a name="input_organizational_units"></a> [organizational\_units](#input\_organizational\_units) | Organizational Units | <pre>list(object({<br>    name        = string<br>    parent_name = string<br>  }))</pre> | `[]` | no |
| <a name="input_organizations_accounts"></a> [organizations\_accounts](#input\_organizations\_accounts) | Organizations Accounts | <pre>list(object({<br>    name        = string<br>    email       = string<br>    parent_name = string<br>    role_name   = string<br>  }))</pre> | `[]` | no |
| <a name="input_organizations_policy"></a> [organizations\_policy](#input\_organizations\_policy) | Organizations Policies | <pre>list(object({<br>    name        = string<br>    description = string<br>    type        = string<br>    content     = string<br>  }))</pre> | `[]` | no |
| <a name="input_policy_id"></a> [policy\_id](#input\_policy\_id) | The unique identifier (ID) of the policy that you want to attach to the target | `string` | `""` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags | `map(string)` | `{}` | no |
| <a name="input_target_id"></a> [target\_id](#input\_target\_id) | The unique identifier (ID) of the root, organizational unit, or account number that you want to attach the policy to | `any` | `[]` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
